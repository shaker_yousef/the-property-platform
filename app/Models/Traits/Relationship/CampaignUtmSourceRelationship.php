<?php

namespace App\Models\Traits\Relationship;

use App\Models\Campaign;

trait CampaignUtmSourceRelationship
{

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}
