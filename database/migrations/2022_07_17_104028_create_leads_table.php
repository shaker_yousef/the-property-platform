<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile_number');
            $table->string('additional_mobile_number')->nullable();
            $table->string('campaign_utm_source_id');
            $table->string('campaign_utm_medium_id');
            $table->string('campaign_utm_campaign_id');
            $table->unsignedBigInteger('entry_user_id');
            $table->date('creation_date');
            $table->boolean('has_comment')->default(false);
            $table->text('comment');
            $table->string('passportId');
            $table->date('passport_expiry');
            $table->string('emiratesId');
            $table->string('address');
            $table->foreignId('user_id');
            $table->foreignId('lead_source_id');
            $table->foreignId('status_id');
            $table->foreignId('city_id');
            $table->foreignId('project_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('lead_source_id')
                ->references('id')
                ->on('lead_sources')
                ->onDelete('cascade');
            $table->foreign('status_id')
                ->references('id')
                ->on('lead_statuses')
                ->onDelete('cascade');
            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('cascade');
            $table->foreign('project_id')
                ->references('id')
                ->on('lead_projects')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
