<?php

namespace App\Models\Traits\Relationship;

use App\Models\Campaign;

trait CampaignUtmMediumRelationship
{
    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}
