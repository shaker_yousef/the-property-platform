<?php

namespace App\Models;

use App\Models\Traits\Relationship\UserRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory,UserRelationship;
}
